const circles = document.querySelectorAll(".step");
const steps = document.querySelectorAll(".stp");
const inputs = document.querySelectorAll(".step-1 form input");
const plans = document.querySelectorAll(".plan");
const switcher = document.querySelector(".switch");
const addons = document.querySelectorAll(".addons");

let addonSelected = [];
let planObj = { pname: "Arcade", pprice: "$9/mo", ptype: "(Monthly)" };
let currStep = 1;
let currCir = 0;

steps.forEach((step) => {
  const next = step.querySelector(".next");
  const back = step.querySelector(".back");
  const change = document.querySelector('.change');

  if (back) {

    back.addEventListener("click", () => {
      circles[currCir].classList.remove("active");
      document.querySelector(`.step-${currStep}`).style.display = "none";
      currStep--;
      document.querySelector(`.step-${currStep}`).style.display = "flex";
      currCir--;
      circles[currCir].classList.add("active");
    });
  }

  if (change) {

    change.addEventListener('click', () => {
      currStep = 2;
      currCir = 1;
      circles[3].classList.remove("active");
      document.querySelector(`.step-${4}`).style.display = "none";
      document.querySelector(`.step-${2}`).style.display = "flex";
      circles[1].classList.add("active");

    });

  }

  if (next) {
    next.addEventListener("click", () => {

      if (validateForm()) {

        circles[currCir].classList.remove("active");
        document.querySelector(`.step-${currStep}`).style.display = "none";
        currStep++;
        document.querySelector(`.step-${currStep}`).style.display = "flex";
        currCir++;
        circles[currCir].classList.add("active");
      }

      if (currStep === 2) {
        doPlan();
      }

      if (currStep === 3) {
        addonSelection();
      }

      if(currStep === 4) {
        planPrice = [];
        updateSummary();
      }
    });
  }
});

//-------------------------------------------step 1 functionality----------------------------------------

function validateForm() {
  let valid = true;
  let validCount = 0;

  for (let index = 0; index < inputs.length; index++) {

    if (!inputs[index].value) {
      valid = false;
      inputs[index].classList.add("err");
      inputs[index].previousElementSibling.lastElementChild.style.display = "flex";
      inputs[index].style.borderColor = "hsl(354, 84%, 57%)";

    } else {

      if (inputs[index].id === 'name') {
        let info = inputs[index];
        valid = validName(info);

        if (valid) {
          validCount++;
        }
      }

      if (inputs[index].id === 'email') {
        let info = inputs[index];
        valid = validEmail(info);

        if (valid) {
          validCount++;
        }
      }

      if (inputs[index].id === 'phone') {
        let info = inputs[index];
        valid = validNumber(info);

        if (valid) {
          validCount++;
        }
      }
    }

  }

  if (validCount === 3) {
    valid = true;
  } else {
    valid = false;
  }

  return valid;
}

//-------------------------------------------step 1 Name validation ---------------------------------------

function validName(info) {
  let namingChar = /^[a-zA-Z-\s]+$/;
  let errorField = info.previousElementSibling.lastElementChild;

  if (namingChar.test(info.value)) {
    valid = true;
    info.classList.remove("err");
    errorField.style.display = "none";

  } else {
    valid = false;
    errorField.textContent = "Enter valid characters.";
    info.classList.add("err");
    errorField.style.display = "flex";
    info.style.borderColor = "hsl(354, 84%, 57%)";
  }

  return valid;
}

//-------------------------step 1 Email validation ------------------------------------------------

function validEmail(info) {
  let namingChar = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  let errorField = info.previousElementSibling.lastElementChild;

  if (namingChar.test(info.value)) {
    valid = true;
    info.classList.remove("err");
    errorField.style.display = "none";

  } else {
    valid = false;
    errorField.textContent = "Enter valid email.";
    info.classList.add("err");
    errorField.style.display = "flex";
    info.style.borderColor = "hsl(354, 84%, 57%)";
  }

  return valid;
}

//-------------------------------------------step 1 Phone number validation-------------------------------------

function validNumber(info) {
  number = info.value.split(" ").join("");
  let errorField = info.previousElementSibling.lastElementChild;

  if (!isNaN(number) && number.slice(3).length === 10) {
    valid = true;
    info.classList.remove("err");
    errorField.style.display = "none";

  } else {
    valid = false;
    errorField.textContent = "Enter valid phone number.";
    info.classList.add("err");
    errorField.style.display = "flex";
    info.style.borderColor = "hsl(354, 84%, 57%)";
  }

  return valid;
}

//-------------------------------------------step 2 functionality-----------------------------------------

function doPlan() {
  plans.forEach((plan) => {

    plan.addEventListener("click", () => {

      document.querySelector(".selected").classList.remove("selected");
      plan.classList.add("selected");
      const planName = plan.querySelector("h4");
      const planPrice = plan.querySelector(".price");
      planObj.pname = planName.textContent;
      planObj.pprice = planPrice.textContent;
    });
  });
  
  switcher.addEventListener("click", () => {
    const val = switcher.querySelector("input").checked;
  
    if (val) {
      document.querySelector(".mn").classList.remove("sw-active");
      document.querySelector(".yr").classList.add("sw-active");
    } else {
      document.querySelector(".mn").classList.add("sw-active");
      document.querySelector(".yr").classList.remove("sw-active");
    }
    switchPrice(val);
  });
  
  function switchPrice(checked) {

    const prices = document.querySelectorAll(".plan-details .price");
    const frees = document.querySelectorAll(".free");
    
    let online = document.querySelector('.online');
    let storage = document.querySelector('.storage');
    let profile = document.querySelector('.profile');

    const yrPrice = [90, 120, 150];
    const mnPrice = [9, 12, 15];
  
    if (checked) {

      prices[0].textContent = `$${yrPrice[0]}/yr`;
      frees[0].style.display = "block";
  
      prices[1].textContent = `$${yrPrice[1]}/yr`;
      frees[1].style.display = "block";
  
      prices[2].textContent = `$${yrPrice[2]}/yr`;
      frees[2].style.display = "block";
      
      addons.forEach((addon) => {

        if (addon.getAttribute("data-id") === "1"){
          addon.lastElementChild.textContent = "+$10/yr";
          online.firstElementChild.textContent = "+$10/yr";

        }
        else if (addon.getAttribute("data-id") === "2"){
          addon.lastElementChild.textContent = "+$20/yr";
          storage.firstElementChild.textContent = "+$20/yr";
        }
        else {
          addon.lastElementChild.textContent = "+$20/yr";
          profile.firstElementChild.textContent = "+$20/yr";
        }
        
      });
  
    } else {

      prices[0].textContent = `$${mnPrice[0]}/mo`;
      frees[0].style.display = "none";
  
      prices[1].textContent = `$${mnPrice[1]}/mo`;
      frees[1].style.display = "none";
      
      prices[2].textContent = `$${mnPrice[2]}/mo`;
      frees[2].style.display = "none";
      
      addons.forEach((addon) => {

        if (addon.getAttribute("data-id") === "1"){
          addon.lastElementChild.textContent = "+$1/mo";
          online.firstElementChild.textContent = "+$1/mo";

        }
        else if (addon.getAttribute("data-id") === "2"){
          addon.lastElementChild.textContent = "+$2/mo";
          storage.firstElementChild.textContent = "+$2/mo";
        }
        else {
          addon.lastElementChild.textContent = "+$2/mo";
          profile.firstElementChild.textContent = "+$2/mo";
        }
        
      });

    }
  }  
}

//-------------------------------------------step 3 functionality--------------------------------------------

function addonSelection() {
  for (let index = 0; index < addons.length; index++) {
    let addon = addons[index];
    let opt = null;
    
    addon.addEventListener('click', (e) => {
      const val = addon.querySelector("input").checked;

      if (val) {

        opt = addon.firstElementChild;
        addon.classList.add('ad-selected');
        addonSelected.push(opt.nextElementSibling);
        document.querySelector(`.${opt.id}`).style.display = "flex";
       
      } else {

        addon.classList.remove('ad-selected');
        addonSelected.splice(addonSelected.indexOf(opt.nextElementSibling), 1);
        document.querySelector(`.${opt.id}`).style.display = "none";
    
      }
  
    });
  }

}

//-------------------------------------------step 4 functionality------------------------------------

function updateSummary() {

  let planName = document.querySelector(".plan-name");
  let planPrice = document.querySelector(".plan-price");
  let totalBill = document.querySelector(".total-bill span");
  let selAddons = document.querySelector(".sel-addons");

  if (planObj.pprice.slice((planObj.pprice.indexOf('/')+1)) === "yr") {
    planObj.ptype = "(Yearly)";
    planName.textContent = planObj.pname + ' ' + planObj.ptype;
    planPrice.textContent = planObj.pprice;

  } else {
    planObj.ptype = "(Monthly)";
    planName.textContent = planObj.pname + ' ' + planObj.ptype;
    planPrice.textContent = planObj.pprice;
  }

  let planBill = Number(planPrice.textContent.slice(1, planPrice.textContent.indexOf('/')));
  let priceArray = [planBill];

  if(addonSelected.length !== 0) {

    selAddons.style.display = "flex";

    if(selAddons.firstElementChild.style.display === "flex") {
      price = selAddons.firstElementChild.firstElementChild.textContent;
      price = Number(price.slice(2, price.indexOf('/')));
      priceArray.push(price);
    }

    if(selAddons.firstElementChild.nextElementSibling.style.display === "flex") {
      price = selAddons.firstElementChild.nextElementSibling.firstElementChild.textContent;
      price = Number(price.slice(2, price.indexOf('/')));
      priceArray.push(price);
    }

    if(selAddons.lastElementChild.style.display === "flex") {
      price = selAddons.lastElementChild.firstElementChild.textContent;
      price = Number(price.slice(2, price.indexOf('/')));
      priceArray.push(price);
    }

    let sum = priceArray.reduce((acc, curr) => { return acc+=curr ;}, 0);

    if (sum < 30) {
      totalBill.textContent = `+$${sum}/mo`;

    } else {
      totalBill.textContent = `+$${sum}/yr`;
    }
    
  } else {
    priceArray = [planBill];
    totalBill.textContent = `+$${planBill}/mo`;
  }

}
